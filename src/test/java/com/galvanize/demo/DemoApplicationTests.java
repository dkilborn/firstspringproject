package com.galvanize.demo;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions.*;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class DemoApplicationTests {

	@Test
	void contextLoads() {
	}

}

class DemoApplicationTests2{
	@Test
	public void returnNumberShouldReturnOne(){
	    //-----SETUP-----


	    //-----ENACT-----

	    //-----ASSERT-----
		assertEquals(1,DemoApplication.returnNumber());
	    //-----TEARDOWN-----
	}
}
